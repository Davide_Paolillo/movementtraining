﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PopulationManager : MonoBehaviour
{
    [SerializeField] private GameObject person;
    [SerializeField] private int populationSize = 50;

    [SerializeField] private float trialTime = 5.0f;

    public static float elapsed = 0.0f;

    private int generation = 0;

    private List<GameObject> population = new List<GameObject>();

    GUIStyle guiStyle = new GUIStyle();
    private void OnGUI()
    {
        guiStyle.fontSize = 25;
        guiStyle.normal.textColor = Color.white;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));
        GUI.Box(new Rect(0, 0,140, 140), "Stats ", guiStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Generation: " + generation, guiStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time {0:0.00}", elapsed), guiStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + (int)elapsed, guiStyle);
        GUI.EndGroup();
    }

    private void Start()
    {
        GenerateInitialPopulation();
    }

    private void GenerateInitialPopulation()
    {
        for (int i = 0; i < populationSize; i++)
        {
            Vector3 position = new Vector3(this.transform.position.x + Random.Range(-2, 2), this.transform.position.y, this.transform.position.z + Random.Range(-2, 2));

            GameObject child = Instantiate(person, position, Quaternion.identity);
            child.GetComponent<Brain>().Init();
            population.Add(child);
        }
    }

    private void Update()
    {
        elapsed += Time.deltaTime;

        if (elapsed >= trialTime)
            SpawnNewPopulation();
    }

    private GameObject Breed(GameObject father, GameObject mother)
    {
        Vector3 position = new Vector3(this.transform.position.x + Random.Range(-2, 2), this.transform.position.y, this.transform.position.z + Random.Range(-2, 2));

        GameObject child = Instantiate(person, position, Quaternion.identity);
        Brain childBrain = child.GetComponent<Brain>();
        childBrain.Init();
        // Mutation on a child over 100
        if (Random.Range(0, 100) == 0)
            childBrain.Dna.Mutate();
        else
            childBrain.Dna.Combine(father.GetComponent<Brain>().Dna, mother.GetComponent<Brain>().Dna);

        return child;
    }

    private void SpawnNewPopulation()
    {
        elapsed = 0.0f;

        List<GameObject> siblings = population.OrderBy(e => Mathf.Abs(e.transform.position.magnitude) * e.GetComponent<Brain>().TimeAlive).ToList();

        population.Clear();

        for (int i = (siblings.Count / 2) - 1; i < siblings.Count - 1; i++)
        {
            population.Add(Breed(siblings[i], siblings[i+1]));
            population.Add(Breed(siblings[i+1], siblings[i]));
        }

        siblings.ForEach(e => Destroy(e));

        ++generation;
    }
}
