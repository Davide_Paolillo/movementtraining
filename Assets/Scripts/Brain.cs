﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Brain : MonoBehaviour
{
    [SerializeField] private int DNALength = 1;
    [SerializeField] private float timeAlive;
    [SerializeField] private DNA dna;

    private ThirdPersonCharacter character;
    private Vector3 move;
    private bool jump = false;

    private bool alive = true;

    public DNA Dna { get => dna; }
    public float TimeAlive { get => timeAlive; }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "dead")
        {
            alive = false;
        }
    }

    public void Init()
    {
        // Initialize DNA
        // possible traits:
        // 0 forward
        // 1 back
        // 2 left
        // 3 right
        // 4 jump
        // 5 crouch

        // So we'll have n genes where, a single gene has a value, if the value is 0 the bow will run forward, if the value is 4 the bot will jump, and so on ....
        dna = new DNA(DNALength, 6);
        character = GetComponent<ThirdPersonCharacter>();
        timeAlive = 0.0f;
        alive = true;
    }

    private void FixedUpdate()
    {
        float forwardSpeed = 0.0f;
        float lateralSpeed = 0.0f;
        bool crouch = false;

        switch (dna.Genes[0])
        {
            case 0:
                forwardSpeed = 1.0f;
                break;
            case 1:
                forwardSpeed = -1.0f;
                break;
            case 2:
                lateralSpeed = -1.0f;
                break;
            case 3:
                lateralSpeed = 1.0f;
                break;
            case 4:
                jump = true;
                break;
            case 5:
                crouch = true;
                break;
        }

        move = forwardSpeed * Vector3.forward + lateralSpeed * Vector3.right;
        character.Move(move, crouch, jump);
        // set to false here to avoid physics bugs
        jump = false;

        if (alive) 
            timeAlive += Time.deltaTime;
    }
}
