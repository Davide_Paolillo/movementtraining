﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA
{
    private List<int> genes;
    private int dnaLength;
    private int maxValues;

    public List<int> Genes { get => genes; }

    public DNA (int dnaLength, int maxValues)
    {
        this.dnaLength = dnaLength;
        this.maxValues = maxValues;
        this.genes = new List<int>();
        SetRandomGenes();
    }

    public void SetRandomGenes()
    {
        genes.Clear();
        for (int i = 0; i < dnaLength; i++)
        {
            genes.Add(Random.Range(0, maxValues));
        }
    }

    public void SetGene(int pos, int value)
    {
        genes[pos] = value;
    }

    /// <summary>
    /// Combine the first half of the father DNA with the last half of the mother DNA
    /// </summary>
    /// <param name="father">Father DNA</param>
    /// <param name="mother">Mother DNA</param>
    public void Combine(DNA father, DNA mother)
    {
        for (int i = 0; i < dnaLength; i++)
        {
            if (i < dnaLength/2.0f)
            {
                genes[i] = father.Genes[i];
            }
            else
            {
                genes[i] = mother.Genes[i];
            }
        }
    }

    public void Mutate()
    {
        genes[Random.Range(0, dnaLength)] = Random.Range(0, maxValues);
    }

    public int GetGene(int position)
    {
        return genes[position];
    }
}
